<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register Form</title>
    <link rel="stylesheet" href="./assets/css/bootstrap.min.css">
    <style>
        .form-control:focus {
            box-shadow: none;
        }
    </style>
</head>

<body>

    <div class="container ">
        <div class="row justify-content-center mt-5">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12 mt-5 mb-3">
                        <label for="formGroupEmail" class="form-label">Name</label>
                        <input type="text" class="form-control " name="input_name" id="input_name" placeholder="Enter your Name">
                    </div>
                    <div class="col-md-12 mb-3">
                        <label for="formGroupEmail" class="form-label">Email</label>
                        <input type="text" class="form-control " name="input_email" id="input_email" placeholder="Enter your email">
                    </div>
                    <div class="col-md-12 mb-3">
                        <label for="formGroupMobile" class="form-label">Mobile Number</label>
                        <input type="number" class="form-control " name="input_mobile" id="input_mobile" placeholder="Enter your mobile number">
                    </div>
                    <div class="col-md-12 mb-3">
                        <label for="formGroupPassword" class="form-label">Password</label>
                        <input type="password" class="form-control " name="input_password" id="input_password" placeholder="Enter your pasword">
                    </div>
                    <div class="col-md-12 mb-3">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>

        </div>
    </div>





    <script src="./assets/js/bootstrap.min.js"></script>
</body>

</html>
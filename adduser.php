<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Details Form</title>
    <link rel="stylesheet" href="./assets/css/bootstrap.min.css">
    <style>

    </style>
</head>

<body>

    <div class="container mt-5">
        <button type="button" class="btn btn-primary"><a class="text-light text-decoration-none" href="./addusercontent.php">Add User</a></button>
        <div class="table-responsive">
            <table class="table mt-5 ">
                <thead>
                    <tr>
                        <th scope="col">Sl.no</th>
                        <th scope="col">Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Mobile</th>
                        <th scope="col">Password</th>
                        <th scope="col">Operation</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="">
                        <th scope="row">1</th>
                        <td>Siva</td>
                        <td>siva123@gmail.com</td>
                        <td>6758943263</td>
                        <td>123@#siva</td>
                        <td>
                            <button type="button" class="btn btn-primary"><a class="text-light text-decoration-none" href="./addusercontent.php">Update</a></button>
                            <button type="button" class="btn btn-danger ms-1"><a class="text-light text-decoration-none" href="#">Delete</a></button>
                        </td>
                    </tr>

                </tbody>
            </table>
        </div>
    </div>


    <script src="./assets/js/bootstrap.min.js"></script>
</body>

</html>